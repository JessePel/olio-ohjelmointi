package ReadIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jesse Peltola
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ReadAndWriteIO file = new ReadAndWriteIO();
        System.out.println( System.getProperty( "user.dir" ) );
        file.readFile("input.txt");
        file.ReadAndWrite("input.txt", "output.txt");
        file.Write("input.txt", "under.txt");
    }
    
}
