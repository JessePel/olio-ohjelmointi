/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReadIO;
import java.io.*;
import java.util.zip.ZipInputStream;
/**
 *
 * @author Jesse Peltola
 */
public class ReadAndWriteIO {
    
    public void readFile(String name) {
        String output = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(name));
            
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                
            }
            br.close();
        } catch (IOException ex) {
        System.out.println("Error");
    } 
        
    }   
    
    public void ReadAndWrite(String infile, String outfile) {
        String output = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(infile));
            BufferedWriter wr = new BufferedWriter(new FileWriter(outfile));
            while ((output = br.readLine()) != null) {
                wr.write(output + "\n");
                
            }
            br.close();
            wr.close();
        } catch (IOException ex) {
        System.out.println("Error");
    } 
        
    }
    public void Write(String infile, String outfile) {
         String output = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(infile));
            BufferedWriter wr = new BufferedWriter(new FileWriter(outfile));
            while ((output = br.readLine()) != null) {
                if (output.trim().length() > 0 && output.trim().length() < 30 &&  output.contains("v")) {
                    wr.write(output+"\n");
                }
                
                
            }
            br.close();
            wr.close();
        } catch (IOException ex) {
        System.out.println("Error");
    } 
    }
    
    
}
