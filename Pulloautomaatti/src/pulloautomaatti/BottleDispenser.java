package pulloautomaatti;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jesse Peltola
 */
public class BottleDispenser {

    private int bottles;
    ArrayList<Pullo> bottle_arrays = new ArrayList();
    private int money;
    
    public BottleDispenser() {
        bottles = 6;
        money = 0;
        
        for (int i = 0; i < bottles; i++) {
            bottle_arrays.add(new Pullo("Pepsi Max", "Pepsi", 0.5));
        }
        
        /*for (int i = 0; i < bottles/3; i++) {
            bottle_arrays.add(new Pullo("Pepsi Max", "Pepsi", 0.5));
        }
        for (int i = bottles/3; i < bottles/3+2; i++) {
            bottle_arrays.add(new Pullo("Fanta", "Fanta", 1.5));
        }
        for (int i = bottles/3 + 2; i < bottles; i++) {
            bottle_arrays.add(new Pullo("Sprite", "Sprite", 1));
        }*/
    }
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Money was added into the machine!");
    }

    public void buyBottle() {
        if (money >= bottle_arrays.get(0).getPrice()) {
            bottles -= 1;
            System.out.println("KACHUNK! Bottle appeared from the machine!");
        } else {
            System.out.println("Syötä lisää rahaa");
        } 
    }

    public void returnMoney() {
        money = 0;
        System.out.println("Klink klink!! All money gone!");
    }
    public void print() {
        bottle_arrays.forEach((bottle_array) -> {
            System.out.println(bottle_array.getName() + "" + bottle_array.getManufacturer() + "" + bottle_array.getPrice());
        });
    }
    public void deleteBottle() {
        
    }
}

