/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pulloautomaatti;

/**
 *
 * @author Jesse Peltola
 */
public class Pullo {
    private String name;
    private String manufacturer;
    private double total_energy;
    private double size;
    private double price;
    
    
    public Pullo() {
        this.name = "Pepsi Max" ;
        this.manufacturer = "Pepsi";
        this.total_energy = 0.3;
        this.size = 0.5;
        this.price = 1.8;
    }

    
    
    public Pullo(String name, String manuf, double totE) {
        this.name = name;
        this.manufacturer = manuf;
        this.total_energy = totE;
    }
    
    public String getName() {
        return this.name;
    }
    public String getManufacturer() {
        return this.manufacturer;
    }
    public double getEnergy() {
        return this.total_energy;
    }
    public double getPrice() {
        return this.price;
    }
}
